export default function Home() {

	return (

		<main class="h-screen flex items-center px-6 lg:px-32 bg-gray-900 text-white relative">

			<header class="w-full absolute left-0 top-0 p-6 lg:p-32">
				<div class="flex justify-between">
					<div>
						<h1 class="text-3xl font-bold"> @auth </h1>
						<span></span>
					</div>


				</div>


			</header>



			<section class="w-full md:w-9/12 xl:w-8/12">
				<h1 class="sm:text-base md:text-2xl lg:text-8xl font-bold text-stone-50">

					 I'm Lavarel Programmer,IT Geek,Neovim User,Linux Enthusiast<br/>
				</h1>

			</section>


			

			<footer class="absolute right-0 bottom-0 p-6 lg:p-32">
				<p class="font-bold mb-1">Copy Left 2022</p>
				<p>hardyweb.net</p>
			</footer>

		</main>


	)
}
